FROM nginx:alpine3.18

RUN cd /usr/share/nginx/html && \
    wget https://html5up.net/directive/download && \
    unzip -oq download -d ./ && \
    rm download
