# DevOpsEducation


## Задание

1. Создать docker файл таким образом, чтобы в репозитории был только докерфайл а архив вытягивался из интернета https://html5up.net/directive
2. Опубликовать в докерхаб
3. Исходный код положить в gitlab
4. Всю разработку вести в ветке dev, периодически мёржить в main по мере публикации образа в докерхаб.
5. В ветке main должны быть инструкция по запуску образа в формате Markdown

Для проверки задания прислать имя образа и ссылка на публичную репу

## Результат

### Dockerfile
```Dockerfile
FROM nginx:alpine3.18

RUN cd /usr/share/nginx/html && \
    wget https://html5up.net/directive/download && \
    unzip -oq download -d ./ && \
    rm download

```
### Сборка образа
```bash
docker build --pull --rm -f "Dockerfile" -t devops01:latest "."
```

### Запуск образа
```bash
docker run --rm -d -p 80:80/tcp devops01:latest 
```
### Образ - https://hub.docker.com/r/artem79/devops01/tags